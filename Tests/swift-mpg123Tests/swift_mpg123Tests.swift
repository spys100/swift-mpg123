import XCTest
@testable import Libmpg123
import CoreMedia

final class swift_mpg123Tests: XCTestCase {
  var dec:mpg123!
  var inBuf:[UInt8] = []
  override func setUp(){
    dec = mpg123()
    XCTAssertEqual(dec.err, 0)
    XCTAssertNotEqual(dec.mh, nil)
    let path = Bundle.module.path(forResource: "fixtures/sin_44100_1", ofType: "mp2")
    XCTAssertNotNil(path, "fixture not found")
    let inputStream = InputStream(fileAtPath: path!)
    inputStream?.open()
    inBuf = Array(unsafeUninitializedCapacity: 2000){ buffer, initializedCount in
      initializedCount = (inputStream?.read(buffer.baseAddress!, maxLength: buffer.count))!
      
      debugPrint(inputStream?.streamError?.localizedDescription)
      XCTAssertNotEqual(-1,initializedCount)
    }
  }
  override func tearDown() {
    dec.close()
  }

  func testLayer2decode() throws {
    let outSamples:[UInt8] = Array(unsafeUninitializedCapacity: 8000){ buffer, initializedCount in
      XCTAssertEqual(dec.decode(input: inBuf, output: buffer, size: buffer.count),0)
      initializedCount = dec.decode(input: inBuf, output: buffer, size: buffer.count)
      XCTAssertEqual(initializedCount, 4608, "encoded bytes")
    }
    
//    let outStream = OutputStream(toFileAtPath: "/tmp/outtest.raw", append: false)
//    outStream?.open()
//    outStream?.write(outSamples, maxLength: outSamples.count)
  }
  @available(tvOS 13.0, *)
  func testLayer2decodeSampleBuffer() throws {
    var bb:CMBlockBuffer? = nil
    var isb:CMSampleBuffer? = nil
    let pts = CMTime(value: 3456, timescale: 44100)

    let fdesc = try CMFormatDescription(audioStreamBasicDescription: AudioStreamBasicDescription(mSampleRate: 44100, mFormatID: kAudioFormatMPEGLayer2, mFormatFlags: 0, mBytesPerPacket: 2, mFramesPerPacket: 1, mBytesPerFrame: 2, mChannelsPerFrame: 1, mBitsPerChannel: 16, mReserved: 0), layout: nil, magicCookie: nil, extensions: nil)
    
    XCTAssertEqual(CMBlockBufferCreateWithMemoryBlock(allocator: kCFAllocatorDefault, memoryBlock: &inBuf, blockLength: inBuf.count, blockAllocator: kCFAllocatorNull, customBlockSource: nil, offsetToData: 0, dataLength: inBuf.count, flags: 0, blockBufferOut: &bb),0)
    XCTAssertEqual(CMAudioSampleBufferCreateReadyWithPacketDescriptions(allocator: kCFAllocatorDefault, dataBuffer: bb!, formatDescription: fdesc, sampleCount: 1024, presentationTimeStamp: pts, packetDescriptions: [AudioStreamPacketDescription(mStartOffset: 0, mVariableFramesInPacket: 0, mDataByteSize: 1253)], sampleBufferOut: &isb),0)
    var osb = try dec.decode(input: XCTUnwrap(isb))
    osb = try dec.decode(input: XCTUnwrap(isb))
    XCTAssertNotNil(osb)
    osb = try dec.decode(input: XCTUnwrap(isb))
    XCTAssertNotNil(osb)
    XCTAssertEqual(pts, CMSampleBufferGetPresentationTimeStamp(osb!),"presentation timestamp")
    XCTAssertGreaterThan(CMSampleBufferGetDuration(osb!),CMTime(value: 1023, timescale: 44100),"duration")
    XCTAssertLessThan(CMSampleBufferGetDuration(osb!),CMTime(value: 1200, timescale: 44100),"duration")
    let bb_out = CMSampleBufferGetDataBuffer(osb!)
    XCTAssertEqual(CMSampleBufferGetNumSamples(osb!) * 2, CMBlockBufferGetDataLength(bb_out!))
    //debugPrint(osb!)
  }
}
