import CLibmpg123
import CoreMedia

public struct swift_mpg123 {
    public private(set) var text = "Hello, World!"

    public init() {
    }
}

public class mpg123 {
  var mh:OpaquePointer?
  var err:Int32 = 0
  var channels:Int = 0
  var samplerate:Int = 0
  var encoding = mpg123_enc_enum(rawValue: 0)
  var pts = CMTime()
  func printErr(_ err:Int32){
    if err == MPG123_ERR.rawValue {
      let eStr = String(cString: mpg123_strerror(mh))
      print(eStr)
    }
  }
  
  public init(){
    let decoderList = toStringList(mpg123_supported_decoders())
    mh = mpg123_new(decoderList.first, &err); printErr(err)
    mpg123_open_feed(mh); printErr(err)
  }
  
  public func close(){
    let err = mpg123_close(mh); printErr(err)
    mpg123_delete(mh)
  }
  
  public func decode(input:[UInt8], output:inout [UInt8]){
    var actualBytes:Int = 0
    var rv:Int32 = 0
    let outBytes = samplerate > 0 ? output.count : 0
    rv = mpg123_decode(mh, input, input.count, &output, outBytes, &actualBytes)
    if rv == MPG123_NEW_FORMAT.rawValue {
      if getFormat() {
        // re process input but consume it now
        rv = mpg123_decode(mh, input, 0, &output, output.count, &actualBytes)
      }
    }
  }
  
  public func decode(input:[UInt8], output: UnsafeMutableBufferPointer<UInt8>, size:Int) -> Int{
    var actualBytes:Int = 0
    var rv:Int32 = 0
    let outBytes = samplerate > 0 ? size : 0
    rv = mpg123_decode(mh, input, input.count, output.baseAddress, outBytes, &actualBytes)
    if rv == MPG123_NEW_FORMAT.rawValue {
      if getFormat() {
        // re process input but consume it now
        rv = mpg123_decode(mh, input, 0, output.baseAddress, size, &actualBytes)
      }
    }
    printErr(rv)
    return actualBytes
  }
  
  public func decode(input:CMSampleBuffer) -> CMSampleBuffer? {
    var sbufOut:CMSampleBuffer? = nil
    var bbOut:CMBlockBuffer? = nil
    let maxBlockSize = 8192
    var sampleBufferOut = [UInt8]()
    var err:OSStatus
    
    if !pts.isValid {
      pts = CMSampleBufferGetPresentationTimeStamp(input)
    }
    
    // MARK: get pointer and size to input buffer
    guard let inBb = CMSampleBufferGetDataBuffer(input) else { return nil }
    let inSz = CMBlockBufferGetDataLength(inBb)
    var inBuf:UnsafeMutablePointer<CChar>?
    err = CMBlockBufferGetDataPointer(inBb, atOffset: 0, lengthAtOffsetOut: nil, totalLengthOut: nil, dataPointerOut: &inBuf)
    let inBufPtr = UnsafePointer<UInt8>(OpaquePointer(inBuf))

    sampleBufferOut = Array<UInt8>(unsafeUninitializedCapacity: maxBlockSize, initializingWith: { buffer, initializedCount in
      let rc = mpg123_decode(mh, inBufPtr, inSz, buffer.baseAddress , samplerate > 0 ? maxBlockSize : 0, &initializedCount); printErr(rc)
      if rc == MPG123_NEW_FORMAT.rawValue {
        if getFormat() {
          // re process input but consume it now
          let rc = mpg123_decode(mh, inBufPtr, inSz, buffer.baseAddress, maxBlockSize, &initializedCount); printErr(rc)
        }
      }
    })
    if samplerate == 0 { return nil }
    
    // MARK: create and get pointer and size to output buffer
    err = CMBlockBufferCreateWithMemoryBlock(allocator: kCFAllocatorDefault, memoryBlock: nil, blockLength: sampleBufferOut.count, blockAllocator: kCFAllocatorDefault, customBlockSource: nil, offsetToData: 0, dataLength: sampleBufferOut.count, flags: 0, blockBufferOut: &bbOut)
    if bbOut == nil { return nil }
    err = CMBlockBufferAssureBlockMemory(bbOut!)
    
    var output:UnsafeMutablePointer<CChar>? = nil
    err = CMBlockBufferGetDataPointer(bbOut!, atOffset: 0, lengthAtOffsetOut: nil, totalLengthOut: nil, dataPointerOut: &output)
    
    memcpy(output, sampleBufferOut, sampleBufferOut.count)
    
    // MARK: Sample buffer creation
    var signed = false
    var isFloat = false
    var sampleBytes:UInt32 = 0
    switch encoding {
    case MPG123_ENC_SIGNED_8:
      signed = true
      sampleBytes = 1
    case MPG123_ENC_SIGNED_16:
      signed = true
      sampleBytes = 2
    case MPG123_ENC_SIGNED_24:
      signed = true
      sampleBytes = 3
    case MPG123_ENC_SIGNED_32:
      signed = true
      sampleBytes = 4
    case MPG123_ENC_FLOAT_32:
      isFloat = true
      sampleBytes = 4
    case MPG123_ENC_FLOAT_64:
      isFloat = true
      sampleBytes = 8
    default:
      print("Unsupported encoding", encoding)
      return nil
    }
    let asbd = AudioStreamBasicDescription(
      mSampleRate: Double(samplerate),
      mFormatID: kAudioFormatLinearPCM,
      mFormatFlags: (signed ? kAudioFormatFlagIsSignedInteger : 0) | (isFloat ? kAudioFormatFlagIsFloat : 0) | kAudioFormatFlagIsPacked | kAudioFormatFlagsNativeEndian,
      mBytesPerPacket: UInt32(channels) * sampleBytes,
      mFramesPerPacket: 1,
      mBytesPerFrame: UInt32(channels) * sampleBytes,
      mChannelsPerFrame: UInt32(channels),
      mBitsPerChannel: 8 * sampleBytes,
      mReserved: 0)
    
    let numSamples = sampleBufferOut.count / Int(asbd.mBytesPerFrame)
    var audioFmtDesc:CMAudioFormatDescription? = nil
    err = withUnsafePointer(to: asbd) { pAsbd in
      return CMAudioFormatDescriptionCreate(allocator: kCFAllocatorDefault,
                                     asbd: pAsbd,
                                     layoutSize: 0,
                                     layout: nil,
                                     magicCookieSize: 0,
                                     magicCookie: nil,
                                     extensions: nil,
                                     formatDescriptionOut: &audioFmtDesc)
    }
    if err != noErr { print("CMAudioFormatDescriptionCreate", err) }
    if audioFmtDesc == nil { return nil }
  
    //let asPktDesc = AudioStreamPacketDescription(mStartOffset: 0, mVariableFramesInPacket: 0, mDataByteSize: UInt32(actualBytes))

    err = CMAudioSampleBufferCreateReadyWithPacketDescriptions(
        allocator: kCFAllocatorDefault,
        dataBuffer: bbOut!,
        formatDescription: audioFmtDesc!,
        sampleCount: numSamples,
        presentationTimeStamp: pts,
        packetDescriptions: nil,
        sampleBufferOut: &sbufOut)
      
    if err != noErr {
      print("CMAudioSampleBufferCreateReadyWithPacketDescriptions",err)
      return nil
    }
    
    pts = CMTime()
    return sbufOut
  }
  
  deinit {
    close()
  }
  
  fileprivate func toStringList(_ cstrarray: UnsafeMutablePointer<UnsafePointer<Int8>?>) -> [String] {
    var rv:[String] = []
    var cstrs = cstrarray
    while let cstrp = cstrs.pointee {
      cstrs += 1
      rv.append(String(cString: cstrp))
    }
    return rv
  }
  
  fileprivate func getFormat() -> Bool{
    var chn:Int32 = 0
    var enc: Int32 = 0
    var rate:Int = 0
    let mperr = mpg123_getformat(mh, &rate, &chn, &enc)
    if mpg123_errors(mperr) != MPG123_OK { printErr(mperr); return false }
    samplerate = rate
    channels = Int(chn)
    encoding = mpg123_enc_enum(rawValue: UInt32(enc))
    return true
  }
}
