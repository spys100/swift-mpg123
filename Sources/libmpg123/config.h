//
//  config.h
//  
//
//  Created by Carsten Kroll on 25.04.22.
//  - free software under the terms of the LGPL 2.1 see COPYING and AUTHORS files in distribution
//

#define ASMALIGN_BALIGN

#define MPG123_NO_CONFIGURE

/* Define if building with dynamcally linked libmpg123 */
// #define DYNAMIC_BUILD 1

/* Define to indicate that float storage follows IEEE754. */
#define IEEE_FLOAT 1

/* Define to disable 16 bit integer output. */
/* #undef NO_16BIT */

/* Define to disable 32 bit integer output. */
/* define NO_32BIT 1 */

/* Define to disable 8 bit integer output. */
/* #undef NO_8BIT */

/* Define to disable downsampled decoding. */
/* #undef NO_DOWNSAMPLE */

/* Define to disable layer I. */
/* #undef NO_LAYER1 */

/* Define to disable layer II. */
/* #undef NO_LAYER2 */

/* Define to disable layer III. */
/* #undef NO_LAYER3 */

/* Define to disable ntom resampling. */
/* #undef NO_NTOM */

/* Define to disable real output. */
/* define NO_REAL 1 */

/* Define to disable string functions. */
/* #undef NO_STRING */

/* Define to disable warning messages. */
/* #undef NO_WARNING */

#define NO_ID3V2
#define NO_ICY
/* The size of `int32_t' */
#define SIZEOF_INT32_T 4

/* The size of `long' */
#define SIZEOF_LONG __SIZEOF_LONG__

/* The size of `off_t' */
#define SIZEOF_OFF_T 8

/* The size of `size_t' */
#define SIZEOF_SIZE_T __SIZEOF_SIZE_T__

/* The size of `ssize_t' */
#define SIZEOF_SSIZE_T 8

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

#define HAVE_INTTYPES_H
#define HAVE_STDLIB_H
#define HAVE_UNISTD_H
#define HAVE_STRINGS_H
#define HAVE_STRERROR
#define HAVE_DIRENT_H
#define HAVE_SYS_STAT_H

/* use floating point calc */
#define REAL_IS_FLOAT

#ifdef __x86_64
#define OPT_X86_64
#define OPT_AVX
#elif __aarch64__
#define OPT_NEON64
#endif

#define OPT_MULTI

#define WANT_GETCPUFLAGS

#define mpg123_ssize_t ssize_t

#undef DEBUG
